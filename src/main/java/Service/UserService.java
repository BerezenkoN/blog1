package Service;
import dao.UserDao;
import dao.UserDaoImpl;
import dao.UserDaoImpl;
import domain.User;
/**
 * Created by java on 16.01.2017.
 */
public class UserService {


        private static UserDao userDao = new UserDaoImpl();

        public static Long save(User user) {
            return userDao.save(user);
        }

        public static User get(Long id) {
            return userDao.get(id);
        }

        public static boolean isValid(String login, String password) {
            return userDao.list()
                    .stream()
                    .filter(user -> user.getLogin().equals(login) && user.getPassword().equals(password))
                    .count() > 0;
        }

        public static User get(String login, String password) {
            return userDao.list()
                    .stream()
                    .filter(user -> user.getLogin().equals(login) && user.getPassword().equals(password))
                    .findFirst()
                    .get();
        }
}
