package controller;
import domain.User;
import Service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
/**
 * Created by java on 16.01.2017.
 */

    public class LoginServlet extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            req.getRequestDispatcher("/login.jsp").forward(req, resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String action = req.getParameter("action");

            switch (action) {

                case "login": {
                    String login = req.getParameter("login");
                    String password = req.getParameter("password");
                    if (UserService.isValid(login, password)) {
                        User user = UserService.get(login, password);
                        HttpSession session = req.getSession();
                        session.setAttribute("login", user.getLogin());
                        session.setAttribute("password", user.getPassword());
                        session.setAttribute("user_id", user.getId());
                    }
                    else {
                        req.getSession().setAttribute("status", "Incorrect login/password");
                        req.getRequestDispatcher("/login.jsp").forward(req, resp);
                    }
                    break;
                }

                case "register": {
                    resp.sendRedirect("/Blog1/register");
                    break;}
            }

        }
}
