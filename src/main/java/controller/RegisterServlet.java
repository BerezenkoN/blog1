package controller;

import java.io.IOException;
import domain.User;
import Service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Created by java on 16.01.2017.
 */
    public class RegisterServlet extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            req.getRequestDispatcher("/register.jsp").forward(req, resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            String login = req.getParameter("login");
            String password = req.getParameter("password");
            String status = "";

            if (login == "" || password == "") {
                status =  "You should fill all the fields";
            } else if (UserService.save(new User(0l, login, password)) > 0) {
                status = "User created. You can now sign in";
            } else {
                status = "User already exists";
            }

            req.setAttribute("status", status);
            req.getSession().setAttribute("status", status);
            resp.sendRedirect("Blog1/login");

        }

}
