package dao;

import domain.Comment;

import java.util.List;

/**
 * Created by java on 16.01.2017.
 */

    public interface CommentDao {
        int save(Comment comment);
        Comment get(int id);
        List<Comment> list();
    }

