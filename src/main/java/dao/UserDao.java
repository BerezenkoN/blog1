package dao;
import domain.User;

import java.util.List;
/**
 * Created by java on 16.01.2017.
 */
public interface UserDao {


        Long save(User user);

        User get(Long id);

        List<User> list();

    }

