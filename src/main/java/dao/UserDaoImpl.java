package dao;

import domain.User;
import utill.Logger;
import utill.SingleConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;


/**
 * Created by java on 16.01.2017.
 */

    public class UserDaoImpl implements UserDao {

    private Connection connection = SingleConnection.getInstance();

    @Override
    public Long save(User user) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO USERS (login, password) " +
                        "VALUES (?, ?, ?) " +
                        "RETURNING id;"
        )) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            Long id = resultSet.getLong("id");
            resultSet.close();
            return id;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public int update(User user) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "UPDATE users SET " +
                        "login = ?, " +
                        "password = ?, " +
                        "WHERE id = ?;"
        )) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPassword());
            statement.setLong(3, user.getId());
            return statement.executeUpdate();
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return -1;
    }

    @Override
    public int delete(Long id) {
        if (connection == null) return -1;
        try (PreparedStatement statement = connection.prepareStatement(
                "DELETE FROM users WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            return statement.executeUpdate();
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return -1;
    }

    @Override
    public User get(Long id) {
        if (connection == null) return null;
        try (PreparedStatement statement = connection.prepareStatement(
                "SELECT * FROM users WHERE id = ?;"
        )) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            User user = new User(
                    resultSet.getLong("id"),
                    resultSet.getString("login"),
                    resultSet.getString("password")

            );
            resultSet.close();
            return user;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

    @Override
    public List<User> list() {
        if (connection == null) return null;
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users;");
            List<User> users = new ArrayList<>();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getLong("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password")

                );
                users.add(user);
            }
            resultSet.close();
            return users;
        } catch (SQLException e) {
            Logger.log(e.getMessage());
        }
        return null;
    }

}