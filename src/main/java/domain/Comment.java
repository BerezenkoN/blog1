package domain;

import java.sql.Timestamp;

/**
 * Created by java on 16.01.2017.
 */
public class Comment {

    private int id;

    private Timestamp date;
    private String content;
    private int userId;

    public Comment(int id, Timestamp date, int userId, String content, int postId) {
        this.id = id;
        this.date = date;
        this.userId = userId;
        this.content = content;
        this.postId = postId;
    }

    private int postId;


    public int getId() {
        return id;
    }

    public Timestamp getDate() {
        return date;
    }

    public String getContent() {
        return content;
    }

    public int getUserId() {
        return userId;
    }

    public int getPostId() {
        return postId;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "id=" + id +
                ", date=" + date +
                ", content='" + content + '\'' +
                ", userId=" + userId +
                ", postId=" + postId +
                '}';

    }
}
