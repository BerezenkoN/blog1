package utill;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by java on 16.01.2017.
 */
public class Logger {

    private static Connection connection = SingleConnection.getInstance();

    public static void log(String text) {
        if (connection == null) return;
        try (PreparedStatement statement = connection.prepareStatement(
                "INSERT INTO log(text) VALUES (?);");
        ) {
            statement.setString(1, text);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}